'use strict';

var angularAWS = angular.module('AngularAWS', ['ng']);
angularAWS.provider('$AWS', function () {
    this.results;

    this.WebIdentityCredentials = function (Arn, Provider, Token) {
        var aws = AWS.config.credentials;
        aws = new AWS.WebIdentityCredentials({
            RoleArn: Arn,
            ProviderId: Provider, // this is null for Google
            WebIdentityToken: Token
        });
        this.results = aws;
    };
    this.Credentials = function (Access, Secret) {
        var aws = AWS.config.update({
            accessKeyId: Access,
            secretAccessKey: Secret
        });
        this.results = aws;
    };
    this.$get = function() {
      var results = this.results;
      return results;
    };
});